# mthtoken - OZ token table string tokenizer #

This tool is integrated as part of OZ ROM compilation which serves to tokenize MTH strings to compress total space used in the OZ ROM.

## Synopsis ##

**mthtoken [-v] [-tkt tokentablefile] [-scan tokensequensefile] [-r] <textfile> | -s "string constant"**

Tokenize specified textfile, using default 'systokens.bin' token table, or using specified tokens using -tkt option.

Specify **-scan** to use preset token ID scan sequence in text file or string.

De-tokenize specified textfile using **-r** option.

As alternative, tokenize string constant with **-s** option.

If **<textfile>** is omitted, expanded tokens are listed to stdout.

Tokenized text is sent to stdout in assembler DEFM format.

**-v** option enables verbose output.
Redirect to file using: **mthtoken textfile > output.asm**


## Compiling mthtoken ##

Check out the sources via your git client, then simply execute the **compile-mthtoken.sh** script (GNU C compiler must be available on your system).

Alternatively, we have provided a Qt project, for compiling the tool. Qt must have been previously installed on your system. 

Simple type **qmake mthtoken.pro**, then **make**. The execute binary will be located in the bin/ folder.